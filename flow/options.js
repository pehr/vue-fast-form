/**
 * 配置文件的定义
 */
declare type options={
  attr:
    {
      parent:string,//上一层名称
      curr:string //本机名称
    },
  readonly:boolean,//只读属性
  title:string,
  dtype:string,//传入数据类型
  type:string,//控件类型
  showType:string,
  trigger:{//触发状态
    hidden:
    disable:
  },
  event:{//触发事件

  }
  click:function //点击事件

};