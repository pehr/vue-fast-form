# 页面配置说明
```js
[] 行 宽度都是100% ，行里可以包行 [[][]] 标识一行里面有2行
{} 列 需要指定列的宽度 ，span （1-24），列在行里 [{span:12}{span:12}] 标识1行两列，两列的列宽都是50%
[
  [{span:12},{span:12:pageDetails:[{}{}}]
] 
表示：根据元素下有一行，有2列，其中第2列里面有一行2列
 列解析
  {         span: 24, //宽度 ，有行就必须有列
            component: 'el-form-item', // 所放的组件
            property: { //组件需要的属性
              label: '说明内容'
            },
            subs: [// 组件里面的子集，其实相当于没有name的slot，
              {
                component: 'el-input',
                attr: 'addr.city'
              }
            ],
            slot:{ //插槽
              slotname:{ //插槽的Name
                component:''
              }
            }
  }

```