# 页面定义说明
```js
 pageDef: {
        component: 'el-form', // 默认容器类型为div,传入后根元素为此，可以为空
        title: '表单名称',//页面title，有需要用的子组件用，可为空
        keyboard: true, // 页面支持键盘移动，可以为空，目前尚为实现该功能
        showType: 'input',
        dataRelation: //1： 由二维数组组成，第二个数组里，第一个表示 满足什么条件， 第二个表示 关联值发生什么变化
                      //2: {aihao:'zz'} 如果改为 {aihao:'zz'，name:'zhangsan'} 表示这2个条件都满足，则执行数组第2个匹配的表达式
                      //3: {aihao:'zz'} 如果改为 [{aihao:'zz'}，{name:'zhangsan'}] 表示这2个条件满足一个，则执行数组第2个匹配的表达式
          [
            [{ aihao: 'zz' }, { addr: { city: 'yujiatou1' }, age: 15 }]
          ],
        // 数据发生变化，影响配置,适用于修改和新增
        // 配置关系 注意这里 这边不同级别的数据用 字符串中间用.分割，主要是为了和details里面保持一致，上面用点的形式是为了和
        defRelation: [
          // 1:pageData发生变化，根据组件的attr 匹配项目，来覆盖对应的属性值
          [{ aihao: 'aa' }, { 'addr.link.phone': { component: 'dlabel' } }],
          [{ aihao: 'abc' }, { 'buttons': { data: { add: { name: '新增新增' }, delete: { name: '删除隐藏', show: false } } } }]
        ]
  }
```