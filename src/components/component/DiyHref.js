
/**
 *  单个组件的JS的写法
 */
 
 var getChildrenTextContent = function (children) {
    return children.map(function (node) {
      return node.children
        ? getChildrenTextContent(node.children)
        : node.text
    }).join('')
  }
//单个组件写法
export default {
  name:"baba",
  render: function (createElement) {
    // 创建 kebab-case 风格的 ID
    var headingId = getChildrenTextContent(this.$slots.default)
      .toLowerCase()
      .replace(/\W+/g, '-')
      .replace(/(^-|-$)/g, '')

    return createElement(
      'h' + this.level,
      [
        createElement('a', {
          attrs: {
            name: headingId,
            href: '#' + headingId,
            // bgColo,
            
          }
        }, this.$slots.default)
      ]
    )
  },
  props: {
    level: {
      type: String,
      required: true
    }
  }
}

