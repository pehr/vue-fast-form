//需要对外暴露的
import vd from './layout/Index.vue';
import vdElm from './layout/IndexElm.vue';
//注册单个组件必须在原型上绑定install方法
vd.install = function(Vue) {
  Vue.component(vd.name, vdLayout);
};


vdElm.install = function(Vue) {
  Vue.component(vdElm.name, vdElm);
};

export  {vd,vdElm};
