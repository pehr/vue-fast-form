// 背景颜色提醒
// 支持复杂标签里查找特定属性来操作，也支持直接操作
export default {
  inserted: function(el, binding, vnode) {
    let style = null
    const attr = binding.value.attr
    const data = binding.value.data
    if (el.id === attr) {
      style = el.style
    } else if (el.children[attr]) {
      style = el.children[attr].style
    }
    if (style) {
      if (data > binding.value.directives.bgColorTip.high) {
        style['background-color'] = 'red'
      } else if (data < binding.value.directives.bgColorTip.low) {
        style['background-color'] = 'green'
      } else {
        style['background-color'] = ''
      }
    }
  },
  update: function(el, binding, vnode) {
    let style = null
    const attr = binding.value.attr
    const data = binding.value.data
    debugger
    if (el.id === attr) {
      style = el.style
    } else if (el.children[attr]) {
      style = el.children[attr].style
    }
    if (style) {
      if (data > binding.value.directives.bgColorTip.high) {
        style['background-color'] = 'red'
      } else if (data < binding.value.directives.bgColorTip.low) {
        style['background-color'] = 'green'
      } else {
        style['background-color'] = ''
      }
    }
  }
}
