## 在自定义指令里给dom增加相关属性
1. 创建属性,给dom添加属性
```js
export default {
  inserted: function (el,binding,vnode) {
  var typ=document.createAttribute("v-focus");
  typ.nodeValue="value";
  el.attributes.setNamedItem(typ);
  }

```
## 页面的定义调整
1. 整体页面定义 pageDef
2. 每个字段定义改为二位数组 pageDetails
3. index.vue 里支持本组件的递归调用。 
4. 判断配置为数组则进行一次递归调用，并且输出一个DIV 


## 页面配置{}和[]说明 数组里面只能是要么都是{}，都是列，要么都是[]行, pageDetails可以嵌套，嵌套的值只能是数组 []
1. [[][]] 表示一个占比100%的div里有一列2行的DIV
2. [{},{}] 表示一个占比100%的div里一行2列的DIV
3. [{},[{}]]=[{},{}]  这种写法不正确 ,混搭写法不正确
4. [{},[[],[]]] 这种写法不正确 ,混搭写法不正确
5. [{span:12},{span:12,pageDetails:[[],[]]}] 一行2列，其中第二列里有2行


## 页面模式在index.vue里转换
```js
 if (trans[pageDef.showType]) {//需要转换才显示
        pageDetails.forEach(def => {
          if (def instanceof Array) {
            this.tranDef(pageDef, def)
          } else if (def.pageDetails) {
            this.tranDef(pageDef, def.pageDetails)
          } else if(def.showType instanceof Object){//用自定义的模式来转换
            if(def.showType[pageDef.showType]){
                def.showType = def.showType[pageDef.showType]
            }else{
                def.showType=def.showType['default']
            }
          } else if (trans[pageDef.showType][def.showType]) {//默认模式转换
            def.showType = trans[pageDef.showType][def.showType]
          }
        });
      }
  ```

  ## 组件循环引用，有一边需要异步进行加载,例如：
  ```js
    compoment{index: () => import('./components/Index.vue')}
  ```