import Vue from 'vue'
import App from './App.vue'
// import VDiy from './components/index.js'  //单个组件
import vd from './index.js' // 这个是多个组件的集合
Vue.config.productionTip = false
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'



// 注册VDIY里所有组件
Vue.use(vd)
Vue.use(ElementUI)
new Vue({
  render: h => h(App)
}).$mount('#app')
