/**
 * 显示视图的类型
 */
const viewType ={
    readonly: "readonly", //只读模式
    view: "view", //视图预览模式 和readonly类似
    add: "add", //新增模式
    edit: "edit",//编辑模式
    input: "input"//表单模式 
  }

/**
 * 显示视图的类型 ,客户告选择他的尺寸
 */
const viewSize = {
  PHONE: "phone", //7寸一下
  PAD: "pad", //7-11寸屏幕
  NOTEBOOK: "notebook", // 12-19寸屏幕
  PC: "PC"
}