// vue-cli3 + webpack 打包生成的文件中不包含map文件，我需要在开发环境和生产环境都产出map 做法如下
const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  publicPath: '/',
  runtimeCompiler: true,

  outputDir: 'dist',
  productionSourceMap: true,
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set('_c', resolve('src/components'))
  },

  //   chainWebpack: (config)=>{
  //     config.resolve.alias
  //         .set('@$', resolve('src'))
  //         .set('assets',resolve('src/assets'))
  //         .set('components',resolve('src/components'))
  // },
  // pages: {
  //   index: {
  //     // page 的入口
  //     entry: 'src/index/main.js',
  //     // 模板来源
  //     template: 'public/index.html',
  //     // 在 dist/index.html 的输出
  //     filename: 'index.html',
  //     // 当使用 title 选项时，
  //     // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
  //     title: 'Index Page',
  //     // 在这个页面中包含的块，默认情况下会包含
  //     // 提取出来的通用 chunk 和 vendor chunk。
  //     chunks: ['chunk-vendors', 'chunk-common', 'index']
  //   }
  // },
  devServer: {
    port: 8080,
    open: true, // 配置自动启动浏览器
    https: false,
    hotOnly: false,
    // proxy: null, // 设置代理
    proxy: false,
    before: app => {}
  },
  configureWebpack: {
    resolve: {
      alias: {
        // 修改Vue倍导入时包的路径
        // "vue$": "vue/dist/vue.js"
        vue$: 'vue/dist/vue.esm.js',
        '@': resolve('src')
      }
    }
  }
}
